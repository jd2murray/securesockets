//
// Created by jmurray on 9/29/16.
//

#ifndef PROJECT_CONNECTION_H
#define PROJECT_CONNECTION_H

#include <cstdlib>
#include <cstdio>
#include <zconf.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <memory.h>
#include <netdb.h>
#include <iostream>

class Connection
{
 public:
  Connection(const char *hostname, int port);
  ~Connection();
  ssize_t Read(void *buf, size_t size);
  void Send(const void *msg, ssize_t size);

 private:
  int sd;
  struct hostent *host;
  struct sockaddr_in addr;
};


#endif //PROJECT_CONNECTION_H
