//
// Created by jmurray on 9/29/16.
//

#include <iostream>
#include "ServerConnection.h"

ServerConnection::ServerConnection(char *port)
{
  memset(&_hints, 0, sizeof(struct addrinfo));
  _hints.ai_family = AF_UNSPEC;     /* Return IPv4 and IPv6 choices */
  _hints.ai_socktype = SOCK_STREAM; /* We want a TCP socket */
  _hints.ai_flags = AI_PASSIVE;     /* All interfaces */

  _s = getaddrinfo(NULL, port, &_hints, &_result);
  if (_s != 0)
  {
    std::cerr << "getaddrinfo: " << gai_strerror(_s);
    //fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(_s));
    //TODO throw
    // return -1;
  }

  for (_rp = _result; _rp != NULL; _rp = _rp->ai_next)
  {
    _sfd = socket(_rp->ai_family, _rp->ai_socktype, _rp->ai_protocol);
    if (_sfd == -1)
      continue;

    _s = bind(_sfd, _rp->ai_addr, _rp->ai_addrlen);
    if (_s == 0)
    {
      // bind success
      break;
    }
    close(_sfd);
  }

  if (_rp == NULL)
  {
    std::cerr << "Could not bind" << std::endl;
    //TODO throw
    //return -1;
  }

  freeaddrinfo(_result);
}

static int _MakeNonBlocking(int sfd)
{
  int flags;
  int s;

  flags = fcntl(sfd, F_GETFL, 0);
  if (flags == -1)
  {
    perror("fcntl");
    return -1;
  }

  flags |= O_NONBLOCK;
  s = fcntl(sfd, F_SETFL, flags);
  if (s == -1)
  {
    perror("fcntl");
    return -1;
  }

  return 0;
}

void ServerConnection::_MakeNonBlocking()
{
  int flags;
  int s;

  flags = fcntl(_sfd, F_GETFL, 0);
  if (flags == -1)
  {
    perror("fcntl");
    //TODO throw
    // return -1;
  }

  flags |= O_NONBLOCK;
  s = fcntl(_sfd, F_SETFL, flags);
  if (s == -1)
  {
    perror("fcntl");
    //TODO throw
    //return -1;
  }

  //return 0;
}

void ServerConnection::_Listen()
{
  //TODO does this need to be a member var?
  _s = listen(_sfd, SOMAXCONN);
  if (_s == -1)
  {
    perror("listen");
    //TODO throw
  }
}

void ServerConnection::_CreateEventQueue()
{
  _efd = epoll_create1(0);
  if (_efd == -1)
  {
    perror("epoll_create");
    abort();
  }

  _event.data.fd = _sfd;
  _event.events = EPOLLIN | EPOLLET;
  _s = epoll_ctl(_efd, EPOLL_CTL_ADD, _sfd, &_event);
  if (_s == -1)
  {
    perror("epoll_ctl");
    abort();
  }

  /* Buffer where _events are returned */
  _events = (epoll_event *) calloc(MAXEVENTS, sizeof _event);
}

void ServerConnection::_ProcessIncomingConnection()
{
  while (1)
  {
    struct sockaddr inAddr;
    socklen_t inLen;
    int infd;
    char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];

    inLen = sizeof inAddr;
    infd = accept(_sfd, &inAddr, &inLen);
    if (infd == -1)
    {
      if ((errno == EAGAIN) ||
          (errno == EWOULDBLOCK))
      {
        //We have processed all incoming
        //connections.
        break;
      }
      else
      {
        perror("accept");
        break;
      }
    }

    _s = getnameinfo(&inAddr, inLen,
                     hbuf, sizeof hbuf,
                     sbuf, sizeof sbuf,
                     NI_NUMERICHOST | NI_NUMERICSERV);
    if (_s == 0)
    {
      printf("Accepted connection on descriptor %d "
                 "(host=%s, port=%s)\n", infd, hbuf, sbuf);
    }

    //Make the incoming socket non-blocking and add it to the
    //list of fds to monitor.
    _s = _MakeNonBlocking(infd);
    if (_s == -1)
      abort();

    _event.data.
        fd = infd;
    _event.
        events = EPOLLIN | EPOLLET;
    _s = epoll_ctl(_efd, EPOLL_CTL_ADD, infd, &_event);
    if (_s == -1)
    {
      perror("epoll_ctl");
      abort();
    }
  }
}

void ServerConnection::_ProcessData(int i)
{
  // We have data on the fd waiting to be read. Read and
  // display it. We must read whatever data is available
  // completely, as we are running in edge-triggered mode
  // and won't get a notification again for the same
  // data.
  int done = 0;

  while (1)
  {
    ssize_t count;
    char buf[512];

    count = read(_events[i].data.fd, buf, sizeof buf);
    if (count == -1)
    {
      // If errno == EAGAIN, that means we have read all
      // data. So go back to the main loop.
      if (errno != EAGAIN)
      {
        perror("read");
        done = 1;
      }
      break;
    }
    else if (count == 0)
    {
      // End of file. The remote has closed the
      // connection.
      done = 1;
      break;
    }

    write(_events[i].data.fd, buf, count);
    /* Write the buffer to standard output */
    _s = write(1, buf, count);
    if (_s == -1)
    {
      perror("write");
      abort();
    }
  }

  if (done)
  {
    printf("Closed connection on descriptor %d\n",
           _events[i].data.fd);

    /* Closing the descriptor will make epoll remove it
       from the set of descriptors which are monitored. */
    close(_events[i].data.fd);
  }
}

//TODO only handle incoming connections?
void ServerConnection::ProcessEvents()
{
  int numPendingEvents;
  int eventIndex;

  numPendingEvents = epoll_wait(_efd, _events, MAXEVENTS, -1);
  for (eventIndex = 0; eventIndex < numPendingEvents; eventIndex++)
  {
    if ((_events[eventIndex].events & EPOLLERR) ||
        (_events[eventIndex].events & EPOLLHUP) ||
        (!(_events[eventIndex].events & EPOLLIN)))
    {
      // An error has occured on this fd, or the socket is not
      // ready for reading (why were we notified then?)
      std::cerr << "epoll error" << std::endl;
      close(_events[eventIndex].data.fd);
      continue;
    }

    else if (_sfd == _events[eventIndex].data.fd)
    {
      // We have a notification on the listening socket, which
      // means one or more incoming connections. */
      _ProcessIncomingConnection();
      continue;
    }
    else
    {
      _ProcessData(eventIndex);
    }
  }
}

ServerConnection::~ServerConnection()
{
  free(_events);
  close(_sfd);
}


