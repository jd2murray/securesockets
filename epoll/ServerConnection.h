//
// Created by jmurray on 9/29/16.
//

#ifndef PROJECT_SERVERCONNECTION_H
#define PROJECT_SERVERCONNECTION_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <errno.h>

#define MAXEVENTS 64

class ServerConnection
{
 public:
  ServerConnection(char* port);
  ~ServerConnection();
  void _MakeNonBlocking();
  void _Listen();
  void _CreateEventQueue();
  void ProcessEvents();

 private:
  void _ProcessIncomingConnection();
  void _ProcessData(int i);

  struct addrinfo _hints;
  struct addrinfo *_result;
  struct addrinfo *_rp;
  int _s;
  int _sfd;
  int _efd;

  //TODO should this be global?
  struct epoll_event _event;
  struct epoll_event *_events;
};


#endif //PROJECT_SERVERCONNECTION_H
