project(epollserver)

SET(SOURCE_FILES_EPOLL
        epoll_server.cpp)

SET(SOURCE_FILES_EPOLL_RF
        ServerConnection.cpp
        epoll_server_refactor.cpp)

SET(SOURCE_FILES_NO_AUTH_CLIENT
        Connection.cpp
        noauth_client.cpp)

SET(LIBS
        ssl
        crypto)

#add_executable(epoll_server ${SOURCE_FILES_EPOLL})
#add_executable(epoll_serverrf ${SOURCE_FILES_EPOLL_RF})
#add_executable(noauth_client ${SOURCE_FILES_NO_AUTH_CLIENT})
#
#target_link_libraries(epoll_server ${LIBS})
#target_link_libraries(epoll_serverrf ${LIBS})
#target_link_libraries(noauth_client ${LIBS})
