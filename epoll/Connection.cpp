//
// Created by jmurray on 9/29/16.
//

#include "Connection.h"

Connection::Connection(const char *hostname, int port)
{
  if ((host = gethostbyname(hostname)) == NULL)
  {
    //TODO throw here
    perror(hostname);
    abort();
  }
  sd = socket(PF_INET, SOCK_STREAM, 0);
  bzero(&addr, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = *(long *) (host->h_addr);
  if (connect(sd, (struct sockaddr *) &addr, sizeof(addr)) != 0)
  {
    close(sd);
    //TODO throw here
    perror(hostname);
    abort();
  }
}

Connection::~Connection()
{
  close(sd);
}

void Connection::Send(const void* msg, ssize_t size)
{
  send(sd, msg, size, 0);
}

ssize_t Connection::Read(void *buf, size_t size)
{
  return read(sd, buf, size);
}


