//
// Created by jmurray on 9/29/16.
//

#include <cstdlib>
#include <cstdio>
#include "ServerConnection.h"
int main(int argc, char **argv)
{
  if (argc != 2)
  {
    fprintf(stderr, "Usage: %s [port]\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  {
    //this will create and bind a socket and
    //and initialize the socket descriptor
    ServerConnection serverConnection(argv[1]);
    serverConnection._MakeNonBlocking();
    serverConnection._Listen();
    serverConnection._CreateEventQueue();

    while (1)
    {
      serverConnection.ProcessEvents();
    }
  }
  return EXIT_SUCCESS;
}

