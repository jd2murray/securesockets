//
// Created by jmurray on 9/20/16.
//
#include <cstdlib>
#include <cstdio>
#include <zconf.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <memory.h>
#include <netdb.h>
#include <iostream>
#include "Connection.h"

int main(int argc, char** argv)
{
  char hostname[]="127.0.0.1";
  int port = 5000;

  Connection connection(hostname, port);
  const char* msg = "hello there";
  connection.Send(msg, strlen(msg)+1);
  char readbuffer[100];
  connection.Read((void*)readbuffer, sizeof(readbuffer));
  std::cout << readbuffer << std::endl;

  sleep(1);
}
