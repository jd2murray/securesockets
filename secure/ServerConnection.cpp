//
// Created by jmurray on 9/29/16.
//
#include <iostream>
#include <openssl/ossl_typ.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <stdexcept>
#include "ServerConnection.h"

//TODO change this sig to strings
ServerConnection::ServerConnection(std::string port, std::string certFile, std::string keyFile, std::string CAName)
{
  memset(&_hints, 0, sizeof(struct addrinfo));
  _hints.ai_family = AF_UNSPEC;     /* Return IPv4 and IPv6 choices */
  _hints.ai_socktype = SOCK_STREAM; /* We want a TCP socket */
  _hints.ai_flags = AI_PASSIVE;     /* All interfaces */

  _s = getaddrinfo(NULL, port.c_str(), &_hints, &_result);
  if (_s != 0)
  {
    std::cerr << "getaddrinfo: " << gai_strerror(_s);
    //TODO throw
    // return -1;
  }

  for (_rp = _result; _rp != NULL; _rp = _rp->ai_next)
  {
    _sfd = socket(_rp->ai_family, _rp->ai_socktype, _rp->ai_protocol);
    if (_sfd == -1)
      continue;
    int optval = 1;
    setsockopt(_sfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);

    _s = bind(_sfd, _rp->ai_addr, _rp->ai_addrlen);
    if (_s == 0)
    {
      // bind success
      break;
    }
    close(_sfd);
  }

  if (_rp == NULL)
  {
    std::cerr << "Could not bind" << std::endl;
    //TODO throw
    //return -1;
  }

  freeaddrinfo(_result);

  //initialize ssl
  _InitSSL();
  _LoadCertificates(certFile, keyFile, CAName);
  _MakeNonBlocking();
  _Listen();
  _CreateEventQueue();

  pthread_mutex_init(&_cn_map_mutex, NULL);
}

//A general utility around sockets that is not specific to this class
int ServerConnection::_MakeNonBlocking(int sfd)
{
  int flags;
  int s;

  flags = fcntl(sfd, F_GETFL, 0);
  if (flags == -1)
  {
    perror("fcntl");
    return -1;
  }

  flags |= O_NONBLOCK;
  s = fcntl(sfd, F_SETFL, flags);
  if (s == -1)
  {
    perror("fcntl");
    return -1;
  }

  return 0;
}

//Use the general makenonblocking call
void ServerConnection::_MakeNonBlocking()
{
  int flags;
  int s;

  flags = fcntl(_sfd, F_GETFL, 0);
  if (flags == -1)
  {
    perror("fcntl");
    //TODO throw
    // return -1;
  }

  flags |= O_NONBLOCK;
  s = fcntl(_sfd, F_SETFL, flags);
  if (s == -1)
  {
    perror("fcntl");
    //TODO throw
    //return -1;
  }
}

void ServerConnection::_Listen()
{
  //TODO does this need to be a member var?
  _s = listen(_sfd, SOMAXCONN);
  if (_s == -1)
  {
    perror("listen");
    //TODO throw
  }
}

//TODO should this be hidden from the user?
//TODO all the user cares about is that the class listens to connections and can send messages to a specified
//TODO recipient
void ServerConnection::_CreateEventQueue()
{
  _efd = epoll_create1(0);
  if (_efd == -1)
  {
    perror("epoll_create");
    abort();
  }
  _event.data.fd = _sfd;
  _event.events = EPOLLIN | EPOLLET;
  _s = epoll_ctl(_efd, EPOLL_CTL_ADD, _sfd, &_event);
  if (_s == -1)
  {
    perror("epoll_ctl");
    abort();
  }

  //shutdown pipe
  pipe(shutdown_pipe);
  _event.data.fd = shutdown_pipe[0];
  _event.events = EPOLLIN;
  _s = epoll_ctl(_efd, EPOLL_CTL_ADD, shutdown_pipe[0], &_event);
  if (_s == -1)
  {
    perror("epoll_ctl");
    abort();
  }

  /* Buffer where _events are returned */
  _events = (epoll_event *) calloc(MAXEVENTS, sizeof _event);
}

void ServerConnection::_ProcessIncomingConnection()
{
  while (1)
  {
    //std::cout << "start of process incomming loop" << std::endl;
    struct sockaddr inAddr;
    socklen_t inLen;
    int inSock;
    char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];

    inLen = sizeof inAddr;
    inSock = accept(_sfd, &inAddr, &inLen);
    //JM TODO create a ssl context and link this descriptor to it
    //store ssl context this socket descriptor in a map
    //do an ssl accept
    //will an event on this descriptor be the same as an event on the ssl descriptor?
    if (inSock == -1)
    {
      if ((errno == EAGAIN) ||
          (errno == EWOULDBLOCK))
      {
        //We have processed all incoming
        //connections.
        break;
      }
      else
      {
        perror("accept");
        break;
      }
    }

    //JM this goes in the event so that we can pull ssl handle on event notification
    //JM new for secure ssl
    SSL *ssl = SSL_new(sslCtx);
    {
      //TODO how should this be destroyed
      SSL_set_fd(ssl, inSock);                        /* set connection socket to SSL state */
      //std::cout << "making ssl accept" << std::endl;
      if (SSL_accept(ssl) == -1)                    /* do SSL-protocol accept */
      {
        //TODO throw
        ERR_print_errors_fp(stderr);
       // std::cerr << "could not accept using ssl protocol " << errno << std::endl;
      }
      //std::cout << "done ssl accept" << std::endl;

      //get the GUUID
      //TODO utility function
      X509 *cert;
      cert = SSL_get_peer_certificate(ssl);    /* Get certificates (if available) */
      X509_NAME *subject_name = X509_get_subject_name(cert);
      int nid_CN = OBJ_txt2nid("CN");
      char peer_CN[100];
      X509_NAME_get_text_by_NID(subject_name, nid_CN, peer_CN, 256);
      //std::cout << "peer cn is " << peer_CN << std::endl;

      //keep a map of peercn to ssl handle
      cnToSSD.insert(std::pair<std::string, SSL *>(std::string(peer_CN), ssl));
    }

    _s = getnameinfo(&inAddr, inLen,
                     hbuf, sizeof hbuf,
                     sbuf, sizeof sbuf,
                     NI_NUMERICHOST | NI_NUMERICSERV);
    //TODO log here
    /*if (_s == 0)
    {
      printf("Accepted connection on descriptor %d "
                 "(host=%s, port=%s)\n", inSock, hbuf, sbuf);
    }*/

    //Make the incoming socket non-blocking and add it to the
    //list of fds to monitor.
    /*_s = _MakeNonBlocking(inSock);
    std::cout << "made connection nonblocking" << std::endl;
    if (_s == -1)
      abort();*/

    _event.data.ptr = (void *) ssl;
    _event.events = EPOLLIN | EPOLLET;
    _s = epoll_ctl(_efd, EPOLL_CTL_ADD, inSock, &_event);
    if (_s == -1)
    {
      perror("epoll_ctl");
      abort();
    }
  }
}

void ServerConnection::_ProcessData(int eventIndex)
{
  // We have data on the fd waiting to be read. Read and
  // display it. We must read whatever data is available
  // completely, as we are running in edge-triggered mode
  // and won't get a notification again for the same
  // data.
  int done = 0;

  //TODO should pass the event as a param
  SSL *ssl = (SSL *) _events[eventIndex].data.ptr;

  while (1)
  {
    ssize_t count;
    char buf[512];

    //std::cout << "event pointer is " << _events[eventIndex].data.ptr << std::endl;
    //count = read(_events[i].data.fd, buf, sizeof buf);
    count = SSL_read(ssl, buf, sizeof(buf));    /* get request */
    if (count == -1)
    {
      // If errno == EAGAIN, that means we have read all
      // data. So go back to the main loop.
      if (errno != EAGAIN)
      {
        perror("read");
        done = 1;
      }
      break;
    }
    else if (count == 0)
    {
      // End of file. The remote has closed the
      // connection.
      done = 1;
      break;
    }

    //write(_events[i].data.fd, buf, count);
    /* Write the buffer to standard output */
    _s = write(1, buf, count);
    SSL_write(ssl, buf, count);
    if (_s == -1)
    {
      perror("write");
      abort();
    }
  }

  if (done)
  {
    int fd = SSL_get_fd(ssl);
    printf("Closed connection on descriptor %d\n", fd);
    // Closing the descriptor will make epoll remove it
    // from the set of descriptors which are monitored.

    //Pull the cert and remove from map to ssl context
    X509 *cert;
    cert = SSL_get_peer_certificate(ssl);
    X509_NAME *subject_name = X509_get_subject_name(cert);
    int nid_CN = OBJ_txt2nid("CN");
    char peer_CN[100];
    X509_NAME_get_text_by_NID(subject_name, nid_CN, peer_CN, 256);

    //std::cout << "erasing cn " << peer_CN << std::endl;

    cnToSSD.erase(std::string(peer_CN));
    SSL_free(ssl);
    close(fd);
  }
}

int ServerConnection::SendMessage(const std::vector<uint8_t> buf, std::string CN)
{
  //TODO requires mutex
  pthread_mutex_lock(&_cn_map_mutex);
  //Look up the SSL context based on file descriptor
  SSL *sslHandle = NULL;
  try
  {
    sslHandle = cnToSSD.at(CN);
    //std::cout << "got handle " << std::endl;
  }
  catch (const std::out_of_range &oor)
  {
    std::cout << "no cn: " <<  CN << std::endl;
    for (auto item: cnToSSD)
    {
      std::cout << item.first << std::endl;
    }
    pthread_mutex_unlock(&_cn_map_mutex);
    //TODO better exception
    throw std::runtime_error(std::string("no cn: ") + CN);
  }
  pthread_mutex_unlock(&_cn_map_mutex);
  int ret = SSL_write(sslHandle, &buf[0], buf.size());
  if (ret < 0)
  {
    int err = SSL_get_error(sslHandle, ret);
    std::cout << "ssl error is " << err << std::endl;
  }
  return ret;
}

size_t ServerConnection::NumClients()
{
  size_t num_clients = 0;
  //TODO automutex library?
  pthread_mutex_lock(&_cn_map_mutex);
  num_clients = cnToSSD.size();
  pthread_mutex_unlock(&_cn_map_mutex);
  return num_clients;
}

void ServerConnection::ProcessEvents()
{
  //TODO requires mutex
  int numPendingEvents;
  int eventIndex;

  numPendingEvents = epoll_wait(_efd, _events, MAXEVENTS, -1);
  for (eventIndex = 0; eventIndex < numPendingEvents; eventIndex++)
  {
    std::cout << "event type is" << std::endl;
    if ((_events[eventIndex].events & EPOLLERR) ||
        (_events[eventIndex].events & EPOLLHUP) ||
        (!(_events[eventIndex].events & EPOLLIN)))
    {
      // An error has occured on this fd, or the socket is not
      // ready for reading (why were we notified then?)
      std::cerr << "epoll error" << std::endl;
      close(_events[eventIndex].data.fd);
      continue;
    }
    else if (_sfd == _events[eventIndex].data.fd)
    {
      // We have a notification on the listening socket, which
      // means one or more incoming connections. */
      //std::cout << "making connection" << std::endl;
      pthread_mutex_lock(&_cn_map_mutex);
      _ProcessIncomingConnection();
      pthread_mutex_unlock(&_cn_map_mutex);
      //std::cout << "done making connection" << std::endl;
      continue;
    }
    else
    {
      std::cout << "processing data" << std::endl;
      pthread_mutex_lock(&_cn_map_mutex);
      _ProcessData(eventIndex);
      pthread_mutex_unlock(&_cn_map_mutex);
    }
  }
  pthread_mutex_unlock(&_cn_map_mutex);
}

ServerConnection::~ServerConnection()
{
  free(_events);
  close(_sfd);
  pthread_mutex_destroy(&_cn_map_mutex);
}

//TODO does this belong in the server class
void ServerConnection::_InitSSL(void)
{
  SSL_library_init();
  const SSL_METHOD *method;

  OpenSSL_add_all_algorithms();        /* load & register all cryptos, etc. */
  OpenSSL_add_all_ciphers();
  SSL_load_error_strings();            /* load all error messages */
  method = SSLv3_server_method();        /* create new server-method instance */
  sslCtx = SSL_CTX_new(method);            /* create new context from method */
  if (sslCtx == NULL)
  {
    ERR_print_errors_fp(stderr);
    std::cerr << "could not initialize ssl" << std::endl;
    //TODO throw
    abort();
  }
  //TODO throw
}

//TODO this is common between server and client
void ServerConnection::_LoadCertificates(std::string certFile, std::string keyFile, std::string CAName)
{
  // set the local certificate from certFile
  if (SSL_CTX_use_certificate_file(sslCtx, certFile.c_str(), SSL_FILETYPE_PEM) <= 0)
  {
    ERR_print_errors_fp(stderr);
    throw(std::runtime_error("SSL_CTX_use_certificate_file failed"));
  }
  // set the private key from keyFile (may be the same as certFile)
  if (SSL_CTX_use_PrivateKey_file(sslCtx, keyFile.c_str(), SSL_FILETYPE_PEM) <= 0)
  {
    ERR_print_errors_fp(stderr);
    throw(std::runtime_error("SSL_CTX_use_PrivateKey failed"));
  }
  // verify private key
  if (!SSL_CTX_check_private_key(sslCtx))
  {
    fprintf(stderr, "Private key does not match the public certificate\n");
    throw(std::runtime_error("SSL_CTX_check_private_key failed"));
  }

  STACK_OF(X509_NAME) *cert_names;
  cert_names = SSL_load_client_CA_file(CAName.c_str());
  if (cert_names != NULL)
  {
    //TODO throw on each of these
    SSL_CTX_set_client_CA_list(sslCtx, cert_names);
    SSL_CTX_set_verify(sslCtx, SSL_VERIFY_PEER, NULL);
    SSL_CTX_load_verify_locations(sslCtx, CAName.c_str(), NULL);
  }
  else{
    throw(std::runtime_error("SSL_load_client_CA_file failed"));
  }
}

void ServerConnection::InterruptProcessEvent()
{
  char wakeup = 'w';
  write(shutdown_pipe[1], (void*)&wakeup, 1);
}
