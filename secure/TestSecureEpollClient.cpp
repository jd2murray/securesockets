//
// Created by jmurray on 10/10/16.
//


//TODO what if need to broadcast to all devices at once?
#include <dirent.h>
#include "gtest/gtest.h"
#include "ClientConnection.h"
#include "ServerConnection.h"

std::string server_port = "5000";
std::string pki_path = "/home/jmurray/development/securesockets/pki";
std::string client_cert = pki_path + "/client/clientcert.pem";
std::string client_key = pki_path + "/client/clientkeynopass.pem";

std::string client_certs_path = pki_path + "/gencert/certs";
//std::string host_name = "localhost";
std::string host_name = "10.10.1.41";


//TODO pull this test out into its own file along with the corresponding server
//TODO component
TEST(SecureEpollClient, ClientConnectReceiveOneMessage) {
  //keep trying to connect
  ClientConnection *p_client_connection = NULL;

  while (true) {
    try {
      p_client_connection = new ClientConnection(host_name, server_port, client_cert, client_key);
      break;
    }
    catch (std::runtime_error &ex) {
      sleep(1);
    }
  }

  std::vector<uint8_t> buf;
  buf.resize(1024 * 1024);
  size_t num_read = 0;
  while (num_read < 10000000) {
    int read_size = 0;
    try {
      read_size = p_client_connection->ReadMessage(buf);
      if (read_size == 0 || read_size == -1) {
        FAIL();
        continue;
      }
      num_read++;
    }
    catch (std::runtime_error &err) {
      std::cout << "failed read message" << std::endl;
      FAIL();
      continue;
    }
    for (int i = 0; i < read_size; i++) {
      std::cout << buf[i];
    }
    std::cout << std::endl;
    std::cout << "nr:" << num_read << std::endl;
  }

  std::cout << "finished test" << std::endl;

  delete p_client_connection;
}


void GetDirs(const char *path, std::vector<std::string> &files, std::string extension) {
  std::cout << "listing " << path << std::endl;
  DIR *dir;
  struct dirent *ent;
  if ((
      dir = opendir(path)
  ) != NULL) {
    while ((
        ent = readdir(dir)
    ) != NULL) {
      std::string file_name = ent->d_name;
      std::string::size_type find_pos = file_name.find(extension);

      if (find_pos != std::string::npos &&
          (find_pos + extension.length() == file_name.length())) {
        files.push_back(file_name.substr(0, find_pos));
      }
    }
    closedir(dir);
  } else {
    throw std::runtime_error("could not list files");
  }
}

std::vector<std::string> cn_list; //list of cns that certs and keys are named for


std::string server_cert = pki_path + "/server/servercert.pem";
std::string server_key = pki_path + "/server/serverkeynp.pem";
std::string ca = pki_path + "/demoCA/cacert.pem";

bool exit_event_loop = false;
void *EventLoop(void *arg) {
  ServerConnection *p_server_connection = (ServerConnection *) arg;
  while (!exit_event_loop) {
    p_server_connection->ProcessEvents();
  }
  pthread_exit(NULL);
}

size_t num_messages_to_send = 1000;
size_t num_clients = 100;
void RunServer() {
  ServerConnection serverConnection(server_port, server_cert, server_key, ca);
  pthread_t eventLoop;
  pthread_create(&eventLoop, NULL, EventLoop, &serverConnection);

  //iterate over every connection and send a message
  //do this 10001
  size_t num_sent = 0;
  while (num_sent < num_messages_to_send) {
    for (size_t index=0; index<num_clients; index++) {
      std::string guuid = cn_list[index];
      //std::cout << "sending to " << guuid << std::endl;
      while(true) {
        try {
          //TODO make sure that send message does not block?
          //TODO this could back up the server
          //TODO could start thread for this?
          const char *char_message = guuid.c_str();
          std::vector<uint8_t> message(char_message, char_message + strlen(char_message));
          serverConnection.SendMessage(message, guuid);
          break;
        }
        catch (std::runtime_error &err) {
          std::cout << "caught send error!" << std::endl;
          sleep(1);
          continue;
        }
      }
    }
    std::cout << "num sent is " << num_sent << std::endl;
    num_sent++;
  }

  std::cout << "finished sending" << std::endl;

  //wait until all clients have disconnected
  while(serverConnection.NumClients() > 0)
  {
    sleep(1);
  }

  std::cout << "all clients dead" << std::endl;
  exit_event_loop = true;
  serverConnection.InterruptProcessEvent();
  void *p_thread_retval;
  pthread_join(eventLoop, &p_thread_retval);
  std::cout << "thread jointed" << std::endl;
}


TEST(SecureEpollClient, SecureEpollClient_ClientConnectReceiveMessage100Clients_Test) {
  std::vector<ClientConnection *> clients;
  GetDirs(client_certs_path.c_str(), cn_list, ".pem");

  //start up threads that will connect and wait for messages from server
  //need at least 100 certs
  ASSERT_TRUE(cn_list.size() >= 100);

  //start up 10 threads each with a range
  pid_t pid;
  size_t cn_index = 0;

  //the client processes
  //reserve this parent process for the server
  for (size_t proc_index = 0; proc_index < num_clients; proc_index++) {
    pid = fork();
    if (pid != 0) {
      cn_index = proc_index;
      break;
    }
  }

  //parent is the server
  if (pid == 0) {
    RunServer();
    //wait for all children
    //sleep(10);
  }
  else {
    //std::cout << "index is " << cn_index << std::endl;
    std::string client_cert = client_certs_path + std::string("/") + cn_list[cn_index] + std::string(".pem");
    std::string client_key = client_certs_path + std::string("/") + cn_list[cn_index] + std::string(".key");
    //std::cout << "client cert is " << client_cert << std::endl;

    ClientConnection *p_client_connection = NULL;

    while (true) {
      try {
        p_client_connection = new ClientConnection(host_name, server_port, client_cert, client_key);
        break;
      }
      catch (std::runtime_error &ex) {
        sleep(1);
      }
    }

    std::vector<uint8_t> buf;
    buf.resize(1024 * 1024);
    size_t num_read = 0;
    while (num_read < num_messages_to_send) {
      int read_size = 0;
      try {
        read_size = p_client_connection->ReadMessage(buf);
        if (read_size == 0 || read_size == -1) {
          std::cout << "failed to read a message!!!!" << std::endl;
          sleep(1);
          //FAIL();
          continue;
        }
        //std::cout << "read message";
        /*for (int i = 0; i < read_size; i++) {
          std::cout << buf[i];
        }
        std::cout << std::endl;*/
        if (num_read % 1000 == 0) {
          //std::cout << num_read << std::endl;
        }
        num_read++;
      }
      catch (std::runtime_error &err) {
        std::cout << "failed read message" << std::endl;
        //FAIL();
        continue;
      }
      /*for (int i = 0; i < read_size; i++) {
        std::cout << buf[i];
      }
      std::cout << std::endl;
      std::cout << "nr:" << num_read << std::endl;*/
    }
    std::cout << num_read << std::endl;
    std::cout << "client done" << std::endl;
    delete p_client_connection;
  }
}
