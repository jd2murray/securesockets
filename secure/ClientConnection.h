//
// Created by jmurray on 10/7/16.
//

#ifndef PROJECT_CLIENTCONNECTION_H
#define PROJECT_CLIENTCONNECTION_H
#include <string>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <vector>
#include <stdint.h>

class ClientConnection
{
 public:
  ClientConnection(std::string hostname, std::string port, std::string certFile, std::string keyFile);
  ~ClientConnection();
  int ReadMessage(std::vector<uint8_t>& buf);

 private:
  void _InitCTX();
  void _LoadCertificates();
  void _OpenConnection();
  void _ShowCerts();

  SSL_CTX *_ctx;
  std::string _hostname;
  std::string _port;
  std::string _certFile;
  std::string _keyFile;
  int _sd;

  SSL *_ssl;
};


#endif //PROJECT_CLIENTCONNECTION_H
