//
// Created by jmurray on 10/7/16.
//
#include "ClientConnection.h"
#include <unistd.h>
#include <stdint.h>
#include <iostream>

int main(int argc, char **argv)
{
  if (argc != 5)
  {
    printf("Usage: %s <hostname> <portnum> <path to cert pem> <path to key pem>\n", argv[0]);
    return -1;
  }

  std::string hostname = std::string(argv[1]);
  std::string portnum = std::string(argv[2]);
  std::string cert = std::string(argv[3]);
  std::string key = std::string(argv[4]);

  std::vector<uint8_t> buf;
  buf.resize(1024*1024);
  ClientConnection clientConnection(hostname, portnum, cert, key);
  while(1)
  {
    clientConnection.ReadMessage(buf);
    for (int i = 0; i< buf.size(); i++)
    {
      std::cout << buf[i];
    }
  }
  sleep(1);
  return 0;
}

