//
// Created by jmurray on 10/1/16.
//
#include <iostream>
#include <stdexcept>
#include "ServerConnection.h"
int main(int argc, char **argv)
{
  if (argc != 5)
  {
    printf("Usage: %s <portnum> <path to cert pem> <path to key pem> <path to ca cert>\n", argv[0]);
    exit(0);
  }

  //TODO these must be strings
  char *portnum;
  char *keyname;
  char *certnam;
  char *caname;

  portnum = argv[1];
  certnam = argv[2];
  keyname = argv[3];
  caname = argv[4];

  try
  {
    ServerConnection serverConnection(portnum, certnam, keyname, caname);
    while (1)
    {
      serverConnection.ProcessEvents();
    }
  }
  catch (std::runtime_error err)
  {
    std::cout << "failed to init with error " << err.what() << std::endl;
  }
  return 0;
}