//
// Created by jmurray on 10/7/16.
//

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <string>
#include <netdb.h>
#include <unistd.h>
#include <iostream>
#include <stdexcept>
#include "ClientConnection.h"

ClientConnection::ClientConnection(std::string hostname, std::string port, std::string certFile, std::string keyFile) :
    _hostname(hostname)
    , _port(port)
    , _certFile(certFile)
    , _keyFile(keyFile)
{
  SSL_library_init();

  //TODO try for these?
  _InitCTX();
  _LoadCertificates();

  try{
    _OpenConnection();
  }
  catch (std::runtime_error ex){
    SSL_CTX_free(_ctx);
    throw ex;
  }

  _ssl = SSL_new(_ctx);      /* create new SSL connection state */
  SSL_set_fd(_ssl, _sd);    /* attach the socket descriptor */
  if (SSL_connect(_ssl) == -1)   /* perform the connection */
  {
    //TODO cleanup and throw
    ERR_print_errors_fp(stderr);
  }
  _ShowCerts();        /* get any certs */
}

ClientConnection::~ClientConnection()
{
  SSL_free(_ssl);        /* release connection state */
  close(_sd);         /* close socket */
  SSL_CTX_free(_ctx);        /* release context */
}

void ClientConnection::_InitCTX()
{
  const SSL_METHOD *method;
  OpenSSL_add_all_algorithms();  /* Load cryptos, et.al. */
  SSL_load_error_strings();   /* Bring in and register error messages */
  method = SSLv3_client_method();  /* Create new client-method instance */
  _ctx = SSL_CTX_new(method);   /* Create new context */
  if (_ctx == NULL)
  {
    ERR_print_errors_fp(stderr);
    //TODO throw here
    abort();
  }
}

void ClientConnection::_LoadCertificates()
{
  /* set the local certificate from CertFile */
  if (SSL_CTX_use_certificate_file(_ctx, _certFile.c_str(), SSL_FILETYPE_PEM) <= 0)
  {
    ERR_print_errors_fp(stderr);
    //TODO throw
    abort();
  }
  /* set the private key from KeyFile (may be the same as CertFile) */
  if (SSL_CTX_use_PrivateKey_file(_ctx, _keyFile.c_str(), SSL_FILETYPE_PEM) <= 0)
  {
    ERR_print_errors_fp(stderr);
    //TODO throw
    abort();
  }
  /* verify private key */
  if (!SSL_CTX_check_private_key(_ctx))
  {
    fprintf(stderr, "Private key does not match the public certificate\n");
    //TODO throw
    abort();
  }
}

void ClientConnection::_OpenConnection()
{
  struct hostent *host;
  struct sockaddr_in addr;

  if ((host = gethostbyname(_hostname.c_str())) == NULL)
  {
    perror(_hostname.c_str());
    abort();
  }
  _sd = socket(PF_INET, SOCK_STREAM, 0);
  bzero(&addr, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(atoi(_port.c_str()));
  addr.sin_addr.s_addr = *(long *) (host->h_addr);
  if (connect(_sd, (struct sockaddr *) &addr, sizeof(addr)) != 0)
  {
    close(_sd);
    perror(_hostname.c_str());
    //TODO better exception
    throw std::runtime_error("could not connect to server");
  }
}

void ClientConnection::_ShowCerts()
{
  X509 *cert;
  char *line;

  cert = SSL_get_peer_certificate(_ssl); /* get the server's certificate */
  if (cert != NULL)
  {
    //printf("Server certificates:\n");
    line = X509_NAME_oneline(X509_get_subject_name(cert), 0, 0);
    //printf("Subject: %s\n", line);
    free(line);       /* free the malloc'ed string */
    line = X509_NAME_oneline(X509_get_issuer_name(cert), 0, 0);
    //printf("Issuer: %s\n", line);
    free(line);       /* free the malloc'ed string */
    X509_free(cert);     /* free the malloc'ed certificate copy */
  }
  else
    printf("No certificates.\n");
}

int ClientConnection::ReadMessage(std::vector<uint8_t>& buf)
{
  //TODO read the size in uint32, resize the buffer
  //may need to do several large reads
  //TODO time this?
  int ret = SSL_read(_ssl, &buf[0], buf.size());
  if (ret <= 0) {
    int err = SSL_get_error(_ssl, ret);
    std::cout << "ssl error is " << err << std::endl;
    ERR_print_errors_fp(stdout);
    std::cout << "done stack print" << err << std::endl;
    abort();
  }
  return ret;
}
