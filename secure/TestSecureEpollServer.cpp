//
// Created by jmurray on 10/5/16.
//

#include "gtest/gtest.h"
#include "ServerConnection.h"
#include <string>

//create a server connection, verify that the
std::string server_port = "5000";
std::string pki_path = "/home/jmurray/development/securesockets/pki";
std::string server_cert = pki_path + "/server/servercert.pem";
std::string server_key = pki_path + "/server/serverkeynp.pem";
std::string ca = pki_path + "/demoCA/cacert.pem";

TEST(SecureEpollServer, CreateAndDestroyServerConnection) {
  ServerConnection serverConnection(server_port,
                                    server_cert,
                                    server_key,
                                    ca);
}

TEST(SecureEpollServer, BadServerCert) {
  try {
    ServerConnection serverConnection(server_port,
                                      std::string("bad path"),
                                      server_key,
                                      ca);
    FAIL();
  }
  catch (std::runtime_error) {

  }
}

TEST(SecureEpollServer, BadServerKey) {
  try {
    ServerConnection serverConnection(server_port,
                                      server_cert,
                                      "bad key",
                                      ca);
    FAIL();
  }
  catch (std::runtime_error) {

  }
}

TEST(SecureEpollServer, BadCACert) {
  try {
    ServerConnection serverConnection(server_port,
                                      server_cert,
                                      server_key,
                                      "bad ca");
    FAIL();
  }
  catch (std::runtime_error) {

  }
}

bool exit_event_loop = false;
void *EventLoop(void *arg) {
  ServerConnection *p_server_connection = (ServerConnection *) arg;
  while (!exit_event_loop) {
    p_server_connection->ProcessEvents();
  }
  pthread_exit(NULL);
}

//this is the server only
//needs to be run in conjunction with client test
//TODO this can run in the same file as the client component
TEST(SecureEpollServer, ServerSendOneMessage) {
  ServerConnection serverConnection(server_port, server_cert, server_key, ca);
  pthread_t eventLoop;
  pthread_create(&eventLoop, NULL, EventLoop, &serverConnection);

  const char *char_message = "hello caserver cn from server";

  //TODO check for cn that does not exist

  size_t num_sent = 0;
  bool exit_when_client_disconnects = false;
  while (num_sent < 1000001) {
    std::string str_message = std::string(char_message) + std::to_string(num_sent);
    const char *char_message = std::to_string(num_sent).c_str();//str_message.c_str();
    std::cout << char_message << std::endl;
    std::vector<uint8_t> message(char_message, char_message + strlen(char_message));
    try {
      //TODO this should dependable -- SSL_Write errors should not bubble up -- auto retry to send? unless
      //TODO catastrophic
      int bytes_sent = serverConnection.SendMessage(message, "caserver");
      //TODO this happens if "accept"ed socket is non blocking
      //TODO because a WANT_WRITE is requested by SSL SSL_Write
      //TODO is non blocking usefull?
      /*if (bytes_sent == 0 || bytes_sent < 0)
      {
        std::cout << "failed to write message: " << std::to_string(bytes_sent) << std::endl;
        std::cout << std::to_string(errno) << std::endl;
        sleep(5);
        continue;
        break;
      }*/
      num_sent++;
    }
    catch (std::runtime_error &err) {
      std::cout << err.what() << std::endl;
      if (exit_when_client_disconnects) {
        std::cout << "breaking out of loop" << std::endl;
        break;
      }
      sleep(1);
      continue;
    }
    exit_when_client_disconnects = true;
  }

  std::cout << "out of loop" << std::endl;
  exit_event_loop = true;
  int *p_retval_thread;
  pthread_join(eventLoop, (void **) &p_retval_thread);
}
