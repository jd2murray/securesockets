//
// Created by jmurray on 9/29/16.
//

#ifndef PROJECT_SERVERCONNECTION_H
#define PROJECT_SERVERCONNECTION_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <errno.h>
#include <vector>
#include <pthread.h>

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <map>

#define MAXEVENTS 64

//TODO seperate the io from the processing of events (ie, be able to use openssl or plain sockets or regular file descriptors)
class ServerConnection
{
 public:
  ServerConnection(std::string port, std::string certFile, std::string keyFile, std::string CAName);
  ~ServerConnection();
  void _Listen();
  void _CreateEventQueue();
  void ProcessEvents();
  int SendMessage(const std::vector<uint8_t> buf, std::string CN);
  size_t NumClients();
  void InterruptProcessEvent();

 private:
  void _ProcessIncomingConnection();
  void _ProcessData(int eventIndex);
  void _MakeNonBlocking();
  int _MakeNonBlocking(int sfd);

  //security
  void _InitSSL();
  void _LoadCertificates(std::string certFile, std::string keyFile, std::string CAName);

  //socket
  struct addrinfo _hints;
  struct addrinfo *_result;
  struct addrinfo *_rp;
  int _s;
  int _sfd;
  int _efd;
  struct epoll_event _event;
  struct epoll_event *_events;

  //security
  SSL_CTX *sslCtx;

  //cn to ssl descriptor
  std::map<std::string, SSL*> cnToSSD;

  pthread_mutex_t _cn_map_mutex;

  //pipe to send shutdown message to epoll
  int shutdown_pipe[2];
};

#endif //PROJECT_SERVERCONNECTION_H
